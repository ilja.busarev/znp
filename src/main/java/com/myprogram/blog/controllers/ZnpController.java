package com.myprogram.blog.controllers;


import com.myprogram.blog.repo.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ZnpController {
    @Autowired
    private PostRepository postRepository;

    @GetMapping("/znp")
    public String znpMain(Model model) {

        model.addAttribute("title", "Запись на прием");
        return "znp-main";
    }

}
