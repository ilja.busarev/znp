package com.myprogram.blog.models;

import jakarta.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
public class Times {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private LocalDateTime time;

    private boolean available;

    @ElementCollection
    private List<Integer> list;

}
