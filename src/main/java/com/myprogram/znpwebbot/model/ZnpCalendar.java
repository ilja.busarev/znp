package com.myprogram.znpwebbot.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class ZnpCalendar {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Basic
    @Column(name = "fio_spec", nullable = false)
    private String fio_spec;

    @Basic
    @Column(name = "day", nullable = false)
    private String day;

    @Basic
    @Column(name = "time", nullable = false)
    private String time;

    @Basic
    @Column(name = "available", nullable = false)
    private Boolean available;

    @Basic
    @Column(name = "chatIdReserve")
    private long chatIdReserve;



}
