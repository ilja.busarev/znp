package com.myprogram.znpwebbot.repository;

import com.myprogram.znpwebbot.model.Day;
import org.springframework.data.repository.CrudRepository;

public interface DayRepository extends CrudRepository<Day, Long> {

}
