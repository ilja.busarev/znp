package com.myprogram.znpwebbot.repository;

import com.myprogram.znpwebbot.model.SpecFio;
import org.springframework.data.repository.CrudRepository;

public interface SpecFioRepository extends CrudRepository<SpecFio, Long> {
}
