package com.myprogram.znpwebbot.repository;

import com.myprogram.znpwebbot.model.Time;
import org.springframework.data.repository.CrudRepository;

public interface TimeRepository extends CrudRepository<Time,Long> {
}
