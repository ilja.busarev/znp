package com.myprogram.znpwebbot.repository;

import com.myprogram.znpwebbot.model.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {

}
