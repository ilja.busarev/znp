package com.myprogram.znpwebbot.repository;

import com.myprogram.znpwebbot.model.ZnpCalendar;
import org.springframework.data.repository.CrudRepository;

public interface ZnpCalendarRepository extends CrudRepository<ZnpCalendar, Long> {
}
