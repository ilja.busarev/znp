package com.myprogram.znpwebbot.service;

import com.myprogram.znpwebbot.config.BotConfig;
import com.myprogram.znpwebbot.model.*;
import com.myprogram.znpwebbot.repository.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.api.methods.commands.SetMyCommands;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.commands.BotCommand;
import org.telegram.telegrambots.meta.api.objects.commands.scope.BotCommandScopeDefault;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.sql.Timestamp;
import java.util.*;

@Slf4j
@Component
public class TelegramBot extends TelegramLongPollingBot {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private SpecFioRepository specFioRepository;
    @Autowired
    private DayRepository dayRepository;
    @Autowired
    private TimeRepository timeRepository;
    @Autowired
    private ZnpCalendarRepository znpCalendarRepository;

    private boolean addPhone = false;
    private boolean start = false;
    final BotConfig config;

    static final String HELP_TEXT = """
            Вы пользуетесь ботом для запись на прием""";
    static final String ERROR_OCCURRED = "Error occurred: ";

    static final String patternPhone = "^(\\+7)*[0-9]{10}$";

    static String userName = "";

    //запоминаем параметры записи
    static String specFio = "";
    static String specDay = "";
    static String specTime = "";


    @Override
    public String getBotToken() {
        return config.getToken();
    }

    @Override
    public String getBotUsername() {
        return config.getBotName();
    }

    public TelegramBot(BotConfig config) { // установки меню команд бота
        this.config = config;
        List<BotCommand> listOfCommands = new ArrayList<>();
        listOfCommands.add(new BotCommand("/start", "Запустить бота для записи"));
        listOfCommands.add(new BotCommand("/help", "help info"));
        try {
            this.execute(new SetMyCommands(listOfCommands, new BotCommandScopeDefault(), null)); // установки меню команд бота
        } catch (TelegramApiException e) {
            log.error("Error setting bot's command  list " + e.getMessage());
        }
    }


    @Override
    public void onUpdateReceived(Update update) { // реагирование бота на апдейты

        if (update.hasMessage() && update.getMessage().hasText()) {
            String messageText = update.getMessage().getText(); //текст сообщения в лоукейсе
            long chatId = update.getMessage().getChatId(); //получение из апдейта id чата

            if (Objects.equals(messageText, "/start")) { //обработка первого сообщения
                //createCalendar(); //создание основной таблицы на основе других

                start = true;
                addPhone = true;
                registerUser(update.getMessage()); //запускает метод регистрации пользователя
                if (addPhone) addUserPhone(chatId, "");

            } else if (update.hasMessage() && addPhone) {
                addUserPhone(update.getMessage().getChatId(), update.getMessage().getText());
            } else if (update.hasMessage() && update.getMessage().hasText() && start) { //проверка апдейта на наличие сообщения

                if ("/help".equals(messageText)) {
                    prepareAndSendMessage(chatId, HELP_TEXT); //вызов метода prepareAndSendMessage + хелп
                } else {
                    prepareAndSendMessage(chatId,
                            "Выберите действие в меню");
                }
            }

        } else if (update.hasCallbackQuery()) {
            String callBackData = update.getCallbackQuery().getData();


            long messageId = update.getCallbackQuery().getMessage().getMessageId();
            long chatId = update.getCallbackQuery().getMessage().getChatId();

            if (callBackData.equals("Записаться на прием")) {
                executeEditMessageText((int) messageId, chatId, "Выбрана процедура записи на прием");


                addSpec(chatId);
            } else if (callBackData.equals("Проверить мою запись")) {
                executeEditMessageText((int) messageId, chatId, "Ваши записи " + callBackData);
                returnZnp(chatId);
            } else if (callBackData.equals("Удалить мою запись")) {
                executeEditMessageText((int) messageId, chatId, "Выберите запись для удаления");
                deleteZnp(chatId);

            } else if (callBackData.matches("[А-Яа-я]*?\s[А-Яа-я]*?\s[А-Яа-я]*")) {
                executeEditMessageText((int) messageId, chatId, "Доступные дни для " + callBackData);
                specFio = callBackData;
                addDay(chatId, specFio);

            } else if (callBackData.matches("\\d{1,2}")) {
                executeEditMessageText((int) messageId, chatId, "Вы выбрали " + callBackData + " число");
                specDay = callBackData;
                addTime(chatId, specFio, specDay);

            } else if (callBackData.matches("[0-9]{2}:[0-9]{2}")) {
                executeEditMessageText((int) messageId, chatId, "Вы выбрали " + callBackData);
                specTime = callBackData;
                successAddZnp(specFio, specDay, specTime, chatId);

            } else if (callBackData.matches("[А-Яа-я]*?\s[А-Яа-я]*?\s[А-Яа-я]*\\|\\d{1,2}\\|[0-9]{2}:[0-9]{2}")) {
                // "Специалист: " + a.getFio_spec() + ", на " + a.getDay() + " число, в " + a.getTime()
                //("[А-Яа-я]*?\s[А-Яа-я]*?\s[А-Яа-я]*\\d{1,2}[0-9]{2}:[0-9]{2}"))

                System.out.println("ТУТА!");
                String[] a = callBackData.split("\\|");
                deleteZnpComplete(a[0], a[1], a[2]);
                executeEditMessageText((int) messageId, chatId, "Вы удалили запись  " + a[0] + " " + a[1] + " " + a[2]);

            }
        }
    }


    private void addUserPhone(Long chatId, String phone) {

        if (phone.matches(patternPhone)) {

            Iterable<User> users = userRepository.findAll();
            users.forEach(a -> {
                if (a.getChatId().equals(chatId)) a.setPhone(phone);
                userName = a.getFirstName();
            });
            userRepository.saveAll(users);

            startCommandReceived(chatId, userName); //создание меню

            addPhone = false;
        } else {
            prepareAndSendMessage(chatId, "Для начала процедуры записи введите пожалуйста Ваш номер телефона в формате +7");
        }
    }


    private void registerUser(Message message) { //добавление пользователя в базу данный по команде start

        if (userRepository.findById(message.getChatId()).isEmpty()) {
            var chatId = message.getChatId();
            var chat = message.getChat();

            User user = new User();

            user.setChatId(chatId);
            user.setFirstName(chat.getFirstName());
            user.setLastName(chat.getLastName());
            user.setUserName(chat.getUserName());
            user.setRegisteredAt(new Timestamp(System.currentTimeMillis()));

            userRepository.save(user);
            log.info("User saved: " + user);
        } else {
            addPhone = false;
            startCommandReceived(message.getChatId(), message.getChat().getFirstName());
        }
    }


    private void startCommandReceived(long chatId, String name) { //метод обработки команды start
        SendMessage message = new SendMessage();
        message.setChatId(String.valueOf(chatId));
        message.setText("Здравствуйте, " + name + " . Выберите дальнейшее действие!");

        InlineKeyboardMarkup markupInLine = new InlineKeyboardMarkup();


        List<List<InlineKeyboardButton>> rowsInLine = new ArrayList<>();
        List<InlineKeyboardButton> rowInLine = new ArrayList<>();

        InlineKeyboardButton button1 = new InlineKeyboardButton();
        button1.setText("Записаться на прием");
        button1.setCallbackData("Записаться на прием");
        rowInLine.add(button1);


        List<InlineKeyboardButton> rowInLine2 = new ArrayList<>();

        InlineKeyboardButton button2 = new InlineKeyboardButton();
        button2.setText("Проверить мою запись");
        button2.setCallbackData("Проверить мою запись");
        rowInLine2.add(button2);

        List<InlineKeyboardButton> rowInLine3 = new ArrayList<>();

        InlineKeyboardButton button3 = new InlineKeyboardButton();
        button3.setText("Удалить мою запись");
        button3.setCallbackData("Удалить мою запись");
        rowInLine3.add(button3);


        rowsInLine.add(rowInLine);
        rowsInLine.add(rowInLine2);
        rowsInLine.add(rowInLine3);

        markupInLine.setKeyboard(rowsInLine);

        message.setReplyMarkup(markupInLine);
        executeMessage(message);

    }

    private void addSpec(long chatId) { //метод обработки команды start
        SendMessage message = new SendMessage();
        message.setChatId(String.valueOf(chatId));
        message.setText("Выберите специалиста");

        InlineKeyboardMarkup markupInLine = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInLine = new ArrayList<>();

        Iterable<ZnpCalendar> znpCalendars = znpCalendarRepository.findAll();

        Set<String> listSpec = new TreeSet<>();

        for (ZnpCalendar znpCalendar : znpCalendars) {
            listSpec.add(znpCalendar.getFio_spec());
        }

        for (String spec : listSpec) {
            List<InlineKeyboardButton> rowInLine = new ArrayList<>();
            InlineKeyboardButton button = new InlineKeyboardButton();
            button.setText(spec);
            button.setCallbackData(spec);
            rowInLine.add(button);
            rowsInLine.add(rowInLine);
        }

        markupInLine.setKeyboard(rowsInLine);
        message.setReplyMarkup(markupInLine);
        executeMessage(message);

    }

    private void addDay(long chatId, String specFio) { //метод обработки команды start

        SendMessage message = new SendMessage();
        message.setChatId(String.valueOf(chatId));
        message.setText("Выберите день");

        InlineKeyboardMarkup markupInLine = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInLine = new ArrayList<>();

        Iterable<ZnpCalendar> znpCalendars = znpCalendarRepository.findAll();

        Set<String> listDay = new TreeSet<>();

        for (ZnpCalendar znpCalendar : znpCalendars) {
            listDay.add(znpCalendar.getDay());
        }
        List<InlineKeyboardButton> rowInLine = new ArrayList<>();

        for (String spec : listDay) {
            InlineKeyboardButton button = new InlineKeyboardButton();
            button.setText(spec);
            button.setCallbackData(spec);
            rowInLine.add(button);
        }

        rowsInLine.add(rowInLine);
        markupInLine.setKeyboard(rowsInLine);
        message.setReplyMarkup(markupInLine);
        executeMessage(message);

    }

    private void addTime(long chatId, String specFio, String specDay) { //метод обработки команды start
        SendMessage message = new SendMessage();
        message.setChatId(String.valueOf(chatId));
        message.setText("Выберите время");

        InlineKeyboardMarkup markupInLine = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInLine = new ArrayList<>();
        List<InlineKeyboardButton> rowInLine = new ArrayList<>();

        Iterable<ZnpCalendar> znpCalendars = znpCalendarRepository.findAll();
        List<String> listTimes = new ArrayList<>();


        for (ZnpCalendar znpCalendar : znpCalendars) {
            if (znpCalendar.getFio_spec().equals(specFio)
                    && znpCalendar.getDay().equals(specDay)
                    && znpCalendar.getAvailable()) {
                listTimes.add(znpCalendar.getTime());
            }
        }

        listTimes.sort(String::compareTo);

        if (listTimes.isEmpty()) {
            prepareAndSendMessage(chatId, "На данное число нет доступного времени для записи, выберите другой день");
            InlineKeyboardButton button = new InlineKeyboardButton();
            button.setText("Выбрать другое число");
            button.setCallbackData(specFio);
            rowInLine.add(button);

        } else {
            for (String time : listTimes) {
                InlineKeyboardButton button = new InlineKeyboardButton();
                button.setText(time);
                button.setCallbackData(time);
                rowInLine.add(button);
            }
        }


        rowsInLine.add(rowInLine);
        markupInLine.setKeyboard(rowsInLine);
        message.setReplyMarkup(markupInLine);
        executeMessage(message);

    }

    private void executeEditMessageText(int messageId, long chatId, String text) {
        EditMessageText message = new EditMessageText();
        message.setChatId(String.valueOf(chatId));
        message.setText(text);
        message.setMessageId(messageId);

        try {
            execute(message);
        } catch (TelegramApiException e) {
            log.error(ERROR_OCCURRED + e.getMessage());
        }
    }


    private void executeMessage(SendMessage message) { //проверка сообщения перед отправкой
        try {
            execute(message); //выполнить отправку
        } catch (TelegramApiException e) {
            log.error(ERROR_OCCURRED + e.getMessage());
        }
    }

    private void prepareAndSendMessage(long chatId, String textToSend) { // метод по отправке сообщения
        SendMessage message = new SendMessage(); //объект сообщения
        message.setChatId(String.valueOf(chatId)); // установить в обьект id чата
        message.setText(textToSend); //установить в объект сообщения текст для отправки
        executeMessage(message); // выполнить отправку через првоеряющий метода executeMessage
    }

    public void createCalendar() {
        Iterable<SpecFio> specFio = specFioRepository.findAll();
        List<String> listSpec = new ArrayList<>();
        for (SpecFio spec : specFio) {
            listSpec.add(spec.getFio());
        }
        listSpec.sort(String::compareTo);

        Iterable<Day> days = dayRepository.findAll();
        List<String> listDay = new ArrayList<>();
        for (Day day : days) {
            listDay.add(day.getDay());
        }
        listDay.sort(String::compareTo);

        Iterable<Time> times = timeRepository.findAll();
        List<String> listTimes = new ArrayList<>();
        System.out.println(listTimes);
        for (Time time : times) {
            listTimes.add(time.getTime());
        }
        listTimes.sort(String::compareTo);

        for (String spec : listSpec) {
            for (String day : listDay) {
                for (String time : listTimes) {
                    ZnpCalendar znpCalendar = new ZnpCalendar();
                    znpCalendar.setFio_spec(spec);
                    znpCalendar.setDay(day);
                    znpCalendar.setTime(time);
                    znpCalendar.setAvailable(true);
                    znpCalendar.setChatIdReserve(0);
                    znpCalendarRepository.save(znpCalendar);
                }
            }
        }

    }

    private void successAddZnp(String specFio, String specDay, String specTime, long chatId) {
        Iterable<ZnpCalendar> znpCalendars = znpCalendarRepository.findAll();

        for (ZnpCalendar znpCalendar : znpCalendars) {
            if (znpCalendar.getFio_spec().equals(specFio)
                    && znpCalendar.getDay().equals(specDay)
                    && znpCalendar.getTime().equals(specTime)) {
                znpCalendar.setAvailable(false);
                znpCalendar.setChatIdReserve(chatId);
                znpCalendarRepository.save(znpCalendar);
            }
        }
    }

    private void returnZnp(long chatId) {
        Iterable<ZnpCalendar> znpCalendars = znpCalendarRepository.findAll();

        znpCalendars.forEach(a -> {
            if (a.getChatIdReserve() == chatId) prepareAndSendMessage(a.getChatIdReserve(),
                    "Специалист: " + a.getFio_spec() + ", на " + a.getDay() + " число, в " + a.getTime()
            );
        });

    }

    private void deleteZnp(long chatId) {
        SendMessage message = new SendMessage();
        message.setChatId(String.valueOf(chatId));
        message.setText("Выберите время");

        InlineKeyboardMarkup markupInLine = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInLine = new ArrayList<>();
        Iterable<ZnpCalendar> znpCalendars = znpCalendarRepository.findAll();

        znpCalendars.forEach(a -> {
            if (a.getChatIdReserve() == chatId) {
                List<InlineKeyboardButton> rowInLine = new ArrayList<>();
                InlineKeyboardButton button = new InlineKeyboardButton();
                button.setText("Специалист: " + a.getFio_spec() + ", на " + a.getDay() + " число, в " + a.getTime());
                button.setCallbackData(a.getFio_spec() + "|" + a.getDay() + "|" + a.getTime());
                //button.setCallbackData(a.getFio_spec() + a.getDay() + a.getTime());
                rowInLine.add(button);
                rowsInLine.add(rowInLine);
            }
        });
        markupInLine.setKeyboard(rowsInLine);
        message.setReplyMarkup(markupInLine);
        executeMessage(message);

    }

    private void deleteZnpComplete(String fio, String day, String time) {
        Iterable<ZnpCalendar> znpCalendars = znpCalendarRepository.findAll();
        znpCalendars.forEach(a -> {
            if (a.getFio_spec().equals(fio) && a.getDay().equals(day) && a.getTime().equals(time)) {
                a.setChatIdReserve(0);
                a.setAvailable(true);
                znpCalendarRepository.save(a);
            }
        });
    }

}